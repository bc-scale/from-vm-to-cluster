# From VM to Cluster

## Glossary

### Virtual Machine (Virtualization)

A Virtual Machine, or VM and also Node (in Kubernetes world as we will see later), is software that provides a computing resource to **run programs and deploy applications**.

You can **run one or more virtual machines on a physical machine**. Here, virtual machines are not physical hardware but software. Thanks to this, you can run multiple virtual machines on the same host. 

### Container

A container refers to software that contains code and all its dependencies, allowing an application to run reliably across **different computing environments.** 

Within a container, **it contains everything it needs** to run your application. Thus, a containerized app can **run in any OS distribution and infrastructure**.

### Docker

Docker is an open-source framework that automates the deployment of applications in containers that are lightweight and portable.

Docker provides OS-level virtualization to deliver applications in packages as a container. From Linux, Windows, or Mac computers, Docker can package an application and its dependencies in a virtual container.

Docker uses two main artifacts that are essential to container technology. One is the actual container, while the other is the container image - a template upon which a container is realized at runtime.

Docker can help you build and deploy software within containers. With Docker, a developer can create, ship, and run applications. It allows you to create a file called a Dockerfile, which then defines the build process. When given to the `docker build` command, it will make an immutable image.

Try to imagine the Docker image as a close-up photo of the application and all its dependencies. In order to start it up, a developer can use the `docker run` command to run it anywhere. You can also make use of the cloud-based repository called the Docker Hub as a registry to keep and distribute the container images that would have been built.

Since its inception in 2013, Docker has become a leading software / PaaS (Platform-as-a-Service) and trademark for containerization.

Today, containers are the standard de facto, amounting to 84% use in production in 2020, up from 23% recorded in 2016.

The crucial benefit of containers is that applications can be decomposed into self-managed components that can live for as long or short as needed, depending on the demand at that time. **These components can be updated independently across many physical servers without needing to rebuild the whole application**, and components can be shared by multiple applications.

Take a look at the list below. It will help you get an answer to this frequently asked question- what is Docker used for?

-  Proxy requests to and from the containers.
-  Manage container lifecycle.
-  Monitor and log all container activity.
-  Mount shared directories.
-  Put resource limits on containers.
-  Build images using the Docker file format.
-  Push and pull images from registries.

**Major Features**

- Easily configure your application environment.
- Creates containers that are totally independent.
- Resolve the limitations of different hardware and reduce the cost of physical infrastructure needed to run different applications.
- Easy to use for development in a microservice environment making it lightweight and portable software that fits the purpose of microservices.
- Applications run in isolation.
- Occupies less space.
- Easily portable and highly secure.
- Short boot-up time.
- Provides Git-like semantics, such as "docker push", "docker commit" to make it easy for application developers to quickly adopt the new technology and incorporate it in their existing workflows.
- Docker images as immutable layers, enabling immutable infrastructure. Committed changes are stored as an individual read-only layers, making it easy to re-use images and track changes. Layers also save disk space and network traffic by only transporting the updates instead of entire images.

### Kubernetes

Kubernetes is a portable and open-source platform for managing containers (emphasis on the “managing”). 

Kubernetes is a container orchestration technology that represents and manages containers within a web application technology for defining and running containers. Docker, on the other hand, is the technology responsible for making and running containers.

Kubenetes and Docker cannot replace each other. Instead, when you combine the two, you're able to boost the efficiency of application development and deployment.

Kubernetes has been widely adopted by organizations around the globe to manage their infrastructures on-premise and on cloud.

**There's a big catch to all of this: it's complex. Running applications in the cloud is complicated. Running applications inside of containers is even more complicated. Running applications in containers on the cloud adds even more complexity. Running containers in an abstracted environment where they don't care which cloud computer they're running on is really really complicated; and that's what Kubernetes does.**

It's not a good idea to be running out-of-date software for many reasons, but Kubernetes was evolving (and improving) quickly, and our lack of updates meant we were falling behind some of the stabilization improvements that had become available.

Google recognized the potential of the Docker image early on and sought to deliver container orchestration "as-a-service" on the Google Cloud Platform. Google had tremendous experience with containers (they introduced cgroups in Linux) but existing internal container and distributed computing tools like Borg were directly coupled to their infrastructure. So, instead of using any code from their existing systems, **Google designed Kubernetes from scratch to orchestrate Docker containers**. Kubernetes was released in February 2015 with the following goals and considerations.

Kubernetes perform management of containers across many hosts, duplicating containers when needed to handle more requirements, and then scaling back down to save resources when not needed. Because of this, they are a logical enabler of multicloud applications. **Being able to update and scale apps so users are always getting the best experience can make the difference between winning business and losing opportunities.** In the post-COVID-19 era, the online experience has never been more important.

However, it's important to note that Kubernetes does not make containers. Instead, it relies upon a container orchestration technology such as Docker to create them. In other words, Kubernetes and Docker more or less need each other.

**You might ask: what is Kubernetes used for? Take a look at the list below:**

Kubernetes can add these computing features to containers:

- **Auto-scaling**: Kubernetes can adapt automatically to changing tasks by initiating and stopping pods whenever that needs to be done.
- **Rollouts**: Kubernetes supports automated rollouts and rollbacks. This will make seemingly complex procedures like Canary and Blue-Green releases not worth bothering about.
- **Self-healing**: It monitors and restarts containers if they break down.
- **Load-balancing**: requests are allocated to pods that are available.
- **Persistent Storage orchestration**: Managing the containers that run an application, Kubernetes can also manage application data attached to a cluster. Kubernetes allows users to request storage resources without having to know the details of the underlying storage infrastructure. A user is able to mount the network storage system as a local file system.
- **Configuration management and secrets**: This feature allows all classified information such as passwords and keys to be kept under a module named [Secrets in Kubernetes](https://acloudguru.com/hands-on-labs/managing-secrets-in-kubernetes). The Secrets in Kubernetes are usually used when configuring the application without reconstructing the image.

**Major Features**

- Increase your productivity in application management across different environments by deploying containers across AWS, Google Cloud Platform, and Microsoft Azure.
- Empower application developers with a powerful tool for Docker container orchestration without having to interact with the underlying infrastructure.
- **Provide standard deployment interface** and primitives for a consistent app deployment experience and APIs across clouds.
- **Build on a Modular API core** that allows vendors to integrate systems around the core Kubernetes technology.
- Kubernetes was very attractive for application developers, as it reduced their dependency on infrastructure and operations teams. 
- Apply updates to your containers without downtime, ensuring the reliability and availability of your applications.
- **Scale up your infrastructure without having to mind a cost impact.**
- Great community support due to its wide use.
- Extremely flexible and compatible allowing you to use it on your private or public cloud. 
- Kubernetes has become the default cloud native application infrastructure platform. Statistics for the percentage of enterprises running Kubernetes range from about 90% per the CNCF.
- Huge community support.
- Multi-cloud support ([Hybrid Cloud](https://searchcloudcomputing.techtarget.com/definition/hybrid-cloud))
- **Underlying Infrastructure**: One of Kubernetes's key advantages is it works on many different kinds of infrastructure. This can be bare metal servers, virtual machines, public cloud providers, private clouds, and hybrid cloud environments.
- Real-world use cases are available.

#### Kubernetes Architecture

##### Basic Architecture Setup

![Kubernetes Architecture 2](/img/kubernetes-architecture2.png "Kubernetes Architecture 2")

##### Deployments (Scheduler)

![Kubernetes Architecture 4](/img/kubernetes-architecture4.png "Kubernetes Architecture 4")

##### Example of basic Kubernetes Services Platform Setup

![Kubernetes Container Engine Cluster](/img/kubernetes-container-engine-cluster.png "Kubernetes Container Engine Cluster")

##### Kubernetes Resource Map

![Kubernetes Resource Map](/img/kubernetes-resource-map.png "Kubernetes Resource Map")

#### Kubernetes Control Plane

The Kubernetes control plane handles requests to run more (or less) of a specific engine component, and starts to run these components onto wherever spare nodes are available in the cluster. This is what Kubernetes is good at - handling the complex orchestration for you so you can focus on writing other software that does cool things.

The control plane's components make global decisions about the cluster, as well as detecting and responding to cluster events. Kubernetes relies on several administrative services running on the control plane. These services manage aspects, such as cluster component communication, workload scheduling, and cluster state persistence.

#### Cluster

Cluster is a set of nodes (computers) that are managed by Kubernetes.

The collection of nodes is known as a Kubernetes cluster. You can also make specific resources so as to extend the potential of a Kubernetes cluster to meet a certain requirement.

#### Node(s)

- Also known as worker node or compute node.
- A virtual or physical machine that contains the services necessary to run containerized applications.
- A Kubernetes cluster needs at least one worker node, but normally have many.
- The worker node(s) host the Pods that are the components of the application workload.
- Pods are scheduled and orchestrated to run on nodes.
- You can scale up and scale down cluster by adding and removing nodes.

#### Pod(s)
Pods: pods are logical groups of containers that share resources like memory, CPU, storage, and network.

Pods are applications that do work within the cluster. If you're familiar with concepts like Docker containers, a pod is just a grouping of one or more containers. A pod is the smallest “unit of work” that a Kubernetes cluster manages.

Pods, containers, and services (a Kubernetes abstraction that exposes a pod to the network) are hosted within a cluster of one or many computers that are either virtual or real. Kubernetes runs over a number of computers or nodes (as they are known in the Kubernetes language).

When a new pod is scheduled to run within a Kubernetes cluster, the Kubernetes control plane (which manages the entire cluster, all of the nodes, and where the pods are executed on those nodes) decides which node has enough spare capacity to handle the request and starts running the pod on that specific node.

### Virtualization vs. Containerization (Docker)

In a virtualized environment, you can have an entire operating system and one or more applications. For example, you can install VirtualBox to run Windows, all while using a Mac.

In contrast, while on a server with five containerized applications (using Docker, of course), you have only one operating system, with each container sharing the operating system of the server. These containers cannot be written into the server and instead can only write into their own mount. What this tells you is that containers are more lightweight while remaining less resource-intensive.

One final note to make is the difference in terms of the life cycle between both virtualization and containerization. For a shorter life cycle, containers will be a better choice thanks to their fast configuration time and lightweight. On the other hand, virtual machines have a longer life cycle, taking longer to set up and weighing heavier. 

![Docker VM](/img/docker-vm.jpeg "Docker VM")

#### Differences

| **Differences** | **Docker** | **Virtual Machine** |
| --- | --- | --- |
| Operating system | Docker is a container-based model where containers are software packages used for executing an application on any operating system.<br>In Docker, the containers share the host OS kernel.<br>| It is not a container-based model; they use user space along with the kernel space of an OS.<br>It does not share the host kernel.<br>Each workload needs a complete OS or hypervisor.|
| Performance | Docker containers result in high-performance as they use the same operating system with no additional software (like hypervisor).<br>Docker containers can start up quickly and result in less boot-up time. | Since VM uses a separate OS; it causes more resources to be used.<br>Virtual machines don't start quickly and lead to poor performance. |
| Portability | With docker containers, users can create an application and store it into a container image. Then, he/she can run it across any host environment.<br>Docker container is smaller than VMs, because of which the process of transferring files on the host's filesystem is easier. | It has known portability issues. VMs don't have a central hub and it requires more memory space to store data.<br>While transferring files, VMs should have a copy of the OS and its dependencies because of which image size is increased and becomes a tedious process to share data. |
| Speed | The application in Docker containers starts with no delay since the OS is already up and running.<br>These containers were basically designed to save time in the deployment process of an application. | It takes a much longer time than it takes for a container to run applications.<br>To deploy a single application, Virtual Machines need to start the entire OS, which would cause a full boot process. |
| More in-depth understanding | Containers stop working when the “stop command” is executed.<br>It has lots of snapshots as it builds images upon the layers.<br>Images can be version controlled; they have a local registry called Docker hub.<br>It can run multiple containers on a system.<br>It can start multiple containers at a time on the Docker engine. | Virtual machines are always in the running state.<br>Doesn't comprise many snapshots.<br>VM doesn't have a central hub; they are not version controlled.<br>It can run only a limited number of VMs on a system.<br>It can start only a single VM on a VMX. |

### Kubernetes vs Docker: What is the Difference?

Now that you know what Docker and Kubernetes are, it's safe to say that these two different technologies were created to work together. These two are not competing against each other -  both have their own purposes in DevOps and usually are used together.

With that in mind, take a look at the differences:

1. Docker is used to isolate your app into containers. It is used to pack and ship your application. On the contrary, Kubernetes is a container scheduler. Its purpose is to deploy and scale applications.
1. Another major difference between Kubernetes and Docker is that Kubernetes was created to run across a cluster, while Docker runs on a single node.
1. The other difference between Kubernetes and Docker is that Docker can be used on its own, without Kubernetes, but to orchestrate, Kubernetes actually needs a container runtime

Kubernetes is now regarded as the standard for container orchestration and management and orchestration. It offers an infrastructure-level framework for orchestrating containers at scale, as well as managing the developer or user interaction with them. In much the same way, Docker has now become the standard for container development and deployment.

#### Docker vs Kubernetes: in Logging and Monitoring

![Kubernetes vs. Docker](/img/kubernetes-vs-docker.png "Kubernetes vs. Docker")

##### Kubernetes Kibana

![Kubernetes Kibana](/img/kubernetes-kibana.png "Kubernetes Kibana")

##### Logging Flow

![Logging Flow](/img/logging-flow.png "Logging Flow")

## Docker vs. Virtual Machine: Which is a Better Choice?

Global researcher Gartner has predicted that by 2023, more than 50% of companies will adopt Docker containers. However, a serverless container like Docker will have a raise in the revenue from a small base of $465.8 million in 2020 to $944 million in 2024.

## Wrapping up

What we need to understand is that they are not to replace one another. From VMs to Docker to Kubernetes, each has unique features that enable you to combine them, meeting your needs in a more efficient way.

All three are powerful tools for developers and infra administrators to efficiently manage their infrastructure and ensure reliability and scalability.

## AWS Services and Kubernetes

### Amazon VPC

Amazon Virtual Private Cloud (VPC) service lets you provision private, isolated sections of the AWS Cloud and then launch AWS services and other resources onto a virtual network. With a VPC you can define your own IP address range and have complete control over your virtual networking environment, including subnets, and route table definition as well as network gateways.

### VPC networking vs. Kubernetes networking

One of the concepts that may be confusing is the networking. There are a few different networks that you need to be aware of when you're running Kubernetes in AWS.

A VPC has its own networking capabilities and it connects cluster nodes or EC2 instances to each other onto its own subnet. A Kubernetes cluster also has its own network-a pod network-which is separate from a VPC instance network.

Pods (collections of containers) with shared storage/network with a specification for how to run the containers. Pods are generally co-located, and co-scheduled and they run in a shared context. This means that containers within pods share an application model and can also share components through local volumes between related services within an application. Each pod has its own IP that are managed and scheduled by the Kubernetes master node.

But pods between EC2 instances need a way to communicate with each other. The VPC itself provides support for setting routes through the kubenet plugin (deprecated as of 1.8). This is a very basic Linux networking plugin that provides near-native performance throughput for your cluster but it lacks other advanced features such as extensive networking across availability zones, the ability to enforce a security policy and also when using a VPC, you cannot effectively network the cluster since it uses multiple route tables. This is why many people resort to using CNI plugins - an open standard for container communications.

![Networking Weave Net](/img/networking-weave-net.png "Networking Weave Net")

### CNI Plugins

Container Network Plugins (CNI) for Kubernetes provide a lot more features than the basic `kubenet linux` networking plugin does. While you do lose some performance with a CNI overlay network, you gain other things like being able to set security policy rules between your services as well as the ability to connect nodes and pods between high availability (HA) zones if you have a cluster that is larger than 50 nodes.

### Amazon EC2

Amazon Elastic Compute Cloud provides scalable secure instances within a VPC. You can provision a virtual instance with any operating system by choosing one of the many Amazon Machine Images (AMIs) available or create your own AMI for distribution and for your own use.

### EC2 Nodes & Kubernetes

When creating instances for your cluster you'll need to think about the size of the nodes. Even though Kubernetes automatically scales and adjusts to a growing app, the resources set for any EC2 nodes you initially create are static and they cannot be changed afterwards.

### Scaling Nodes

Scaling nodes is not supported through Kubernetes' command-line interface, `kubectl` in AWS. If you need to use Kubernetes autoscaler, then you'll need to do it manually through the AWS with the Autoscaling Group feature or you can also manually create a set number of EC2 nodes to achieve the same result.

### Amazon Route53 for Kubernetes Cluster Setup

Kubernetes clusters need DNS so that the worker nodes can talk to the master as well as discover the etcd and then the rest of its components.

When running Kubernetes in AWS, you can make use of Amazon Route 53 or you can run an external DNS.

If you will be running multiple clusters, each cluster should have its own subdomain as well.

### Load Balancers

There are basically two design patterns in AWS where you may need load balancers:

During the installation of Kubernetes on AWS, 
When you're exposing app services to the outside world and you have deployed more than one master running, you may need to provision an external load balancer so that you have an externally-accessible IP address for your application that is accessible to the outside world.

For more information about finding and exposing an external IP for Kubernetes see the section below on  How to Define Ingress and for more in depth information refer to the topic, Publishing Services in the Kubernetes documentation.

### Amazon EBS

Amazon Elastic Block Store (Amazon EBS) provides persistent block storage volumes for use with EC2 cloud instances. Each Amazon EBS volume is automatically replicated within its Availability Zone to protect you from component failure, offering high availability and durability. Amazon EBS volumes provide consistent and low-latency performance needed to run your workloads.

### Data Stores and Kubernetes

Sometimes pods need persistent data across volumes. For example if some of your containers are MySQL databases (or any databases for that matter), and they crash, having a backup for your persistent volumes ensures that when the MySQL container comes back up, it can resume where it left off.

![Kubernetes Storage](/img/kubernetes-storage.png "Kubernetes Storage")

## Basic Kubernetes with AWS Setup

### Identity & Access Management (IAM)

Kubernetes does not provide specific AWS IAM roles and permissions. If you are storing and retrieving information from an S3 Bucket or from DynamoDB (calls the AWS API directly), then you will need to think about how to provide IAM permissions for your nodes, pods, and containers. Normally you will want different IAM roles for the masters and the nodes.

You could assign a global IAM role to a Kubernetes node, where all of the IAM roles required by all containers and pods running in Kubernetes are automatically inherited. But from a security standpoint, this is not an optimal. Instead, you will need a more granular approach, one that can assign IAM roles at the Pod and the container level and not just at the node level.

If you use kops to set up your cluster two IAM roles are set up for your cluster one for the masters and one for the nodes.

There are a few different approaches to manage the AWS security requirements:

- Group authentication models for applications on Kubernetes and then give groups of nodes certain IAM permissions
- Implement a proxy such as kube2iam
- Use a solution like vaultto handle app-level secrets

## Summary of Kubernetes on AWS

At a high level, these are the issues you need to consider when running Kubernetes on AWS:

| AWS Service                           | Why you need it & what to consider                                                                                                                  |
|---------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------|
| Amazon VPC                            | Use a CNI network for HA clusters with > 50 nodes                                                                                                   |
| EC2                                   | Incorporate capacity planning for node resources<br>Nodes can't be scaled through `kubectl`; needs the autoscaling feature either in the GUI or not |
| Amazon Route53                        | Kubernetes clusters require DNS to discover all of its components<br>Multiple clusters need subdomains                                              |
| Ingress Rules & Elastic Load Balancer | Ingress rules definition & planning<br>Use Amazon's LB<br>NGINX or use the ingress controller provided by the with Kubernetes API                   |
| Amazon EBS                            | Allocate elastic block storage for stateful applications to ensure continuity during downtime                                                       |
| Identity & Access Management (IAM)    | Kubernetes controller needs IAM roles for master and nodes<br>May need finer grain control if you are accessing the AWS API directly                |

![Kubernetes AWS](/img/kubernetes-aws.jpeg "Kubernetes AWS")

---

## Why run Kubernetes on AWS?

AWS is a premier solution for running cloud native apps, but setting up and running Kubernetes on it can be complex. Despite this, there are many reasons to run Kubernetes on AWS. One of the most appealing reasons is to take advantage of the vast number of services that are available.

## A Real-Life Use-Case of Docker

### How Docker resolved issue at BBC (British news channel with over 500 developers working across the globe)

**Challenges**

- BBC uses several languages in different areas of the world because of which it consists of over 10 Continuous Integrations.
- The company had to identify a way to unify the coding processes and monitor the Continuous Integration consistently.
- Also, the existing jobs took up to 60 minutes to schedule and perform its task.

**Solution**

- With the help of containers, the developers were able to work in flexible CI environments.
- Entire code processes were unified and stored in a single place for easy and quick access.
- Docker helped in eliminating job wait time and resulted in speeding up the entire process.

### Red Canary Detection Engine scaled beyond its cluster capacity

> Red Canary processes a lot of data. On average, we process about 1 petabyte (which is 1,000 terabytes, or 1,000,000 gigabytes, or 772 million floppy disks) of incoming security telemetry per day.

> To handle the volume of incoming data, Red Canary needs thousands of instances of each of these components and others, running across hundreds of computers running in the cloud. This is a lot of complexity to manage, so they use Kubernetes to help us out.

> Red Canary and theirs Kubernetes cluster grows by about 300 nodes (computers) per day, adding about 20,000 CPUs - the equivalent of 3,300 MacBook Pros - during peak traffic. 

**Read this story** about ,,[We're going to need a bigger boat: What to do when you outgrow your Kubernetes cluster](https://redcanary.com/blog/kubernetes-cluster/)" ...

### Single Tenant vs Multi-Tenant - What's the Difference?

https://digitalguardian.com/blog/saas-single-tenant-vs-multi-tenant-whats-difference
